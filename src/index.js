import React, { Component } from  "react";
import ReactDOM from "react-dom"
import "./assets/style.css";
import quizService from "./quizService";
import QuestionBox from "./components/QuestionBox";

class QuizBee extends Component {
  state = {
    questionBank: [],
    score: 0,
    responses: 0
  };
  getQuestion = () => {
    quizService().then(question => {
      this.setState({
        questionBank: question
      });
    });
  };
  componentDidMount(){
    this.getQuestion();
  }
  computeAnswer = (answer, correctAnswer) => {
    if (answer === correctAnswer){
      this.setState({
        score: this.state.score + 1
      });

    }
    this.setState({
      responses: this.state.responses < 5 ? this.state.responses + 1 : 5
    });
  };
  render()
  { 
    return (
      <div className="container">
        <div className="title">Quiz Master</div>
        {this.state.questionBank.length > 0 &&
        this.state.responses < 5 &&  
         this.state.questionBank.map(({question, answers, correct, questionID}) => 
        (
        <QuestionBox
         question={question}
         options={answers}
         key={questionID}
         selected={answer => this.computeAnswer(answer, correct)}/> ))}

         {this.state.responses === 5 ? (<h1>You have scored {this.state.score} Out of 5</h1>) : null}
      </div>
    );
  }
}

ReactDOM.render(<QuizBee />, document.getElementById("root"));